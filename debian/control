Source: ebumeter
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <snd@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 13),
 libclthreads-dev,
 libclxclient-dev,
 libjack-dev | libjack-jackd2-dev,
 libfreetype-dev,
 libpng-dev,
 libsndfile1-dev,
 libzita-resampler-dev,
 pkg-config,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/multimedia-team/ebumeter.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/ebumeter
Homepage: https://kokkinizita.linuxaudio.org/linuxaudio/ebumeter-doc/quickguide.html

Package: ebumeter
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: loudness measurement according to EBU-R128
 These tools help controlling loudness of audio material aimed
 primarily for broadcasting, but they can be useful in other
 situation.
 .
 ebumeter and ebur128 allow users to measure Momentary (400 ms),
 Short term (3s) and Integrated Loudness (from start to stop).
 They can be used on audio files and signals through JACK.
 .
 This package contains two programs:
   * ebumeter - interactive meter working via jackd
   * ebur128 - the command line app to measure sound files

Package: ebumeter-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends}
Description: loudness measurement EBU-R128 - documentation
 These tools help controlling loudness of audio material aimed
 primarily for broadcasting, but they can be useful in other
 situation.
 .
 ebumeter and ebur128 allow users to measure Momentary (400 ms),
 Short term (3s) and Integrated Loudness (from start to stop).
 They can be used on audio files and signals through JACK.
 .
 This package contains html documentation
