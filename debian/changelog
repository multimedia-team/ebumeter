ebumeter (0.5.1-1) unstable; urgency=medium

  [ Dennis Braun ]
  * Fix desktop icon

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Dennis Braun ]
  * New upstream version 0.5.1
  * Refresh Makefile patch
  * Change my email address
  * d/control:
    + Bump Standards-Version to 4.6.2
    + Build-Depends: libfreetype6-dev => libfreetype-dev
    + Use libjack-jackd2-dev as optional B-D
  * Bump d/copyright years
  * d/gbp.conf: Remove wrong compression
  * Add salsa ci config
  * d/watch:
  * Use @PACKAGE@-@ANY_VERSION@@ARCHIVE_EXT@
  * Bump the version to 4

 -- Dennis Braun <snd@debian.org>  Wed, 14 Jun 2023 22:40:14 +0200

ebumeter (0.4.2-2) unstable; urgency=medium

  * Add a new icon
  * d/control:
    + Bump dh-compat to 13
    + Bump S-V to 4.5.0
    + Set RRR: no
    + Add me as uploader
  * Update patchset, fix pkgconf installation properly
  * Use install files for installation, no need to override auto install
  * d/rules:
    + Use --sourcedirectory instead of --sourcedir and fix install files
    + Remove obsolete LDFLAGS export

 -- Dennis Braun <d_braun@kabelmail.de>  Sat, 02 May 2020 16:56:53 +0200

ebumeter (0.4.2-1) unstable; urgency=medium

  * Team upload.

  [ Mattia Rizzolo ]
  * d/control: mark ebumeter-doc as Multi-Arch:foreign.

  [ Felipe Sateler ]
  * d/control: change maintainer address to debian-multimedia@lists.debian.org .

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org .
  * Use debhelper-compat instead of debian/compat

  [ Olivier Humbert ]
  * d/ebumeter.desktop: add a French translation for the menu item and comment.
  * d/copyright: add myself in the debian section.
  * http > https in d/control, d/copyright and d/watch

  [ Dennis Braun ]
  * New upstream version
  * d/control:
    + Bump debhelper-compat to 12
    + Bump standards version to 4.4.1
    + Add libfreetype6-dev and pkg-config to Build-Depends
  * d/copyright: Update copyright year and add myself
  * d/docs: Drop, otherwise README is installed twice
  * d/ebumeter.manpages: Fix path
  * d/patches: Update patchset
    + Update 01-makefile.patch
    + Drop 02-makefile.patch, obsolete
    + Add 03-pkg-config_fix.patch
  * d/source/local-options: Drop, obsolete

 -- Dennis Braun <d_braun@kabelmail.de>  Mon, 20 Jan 2020 21:48:32 +0100

ebumeter (0.4.0-4) unstable; urgency=medium

  * Team upload.

  [ Mattia Rizzolo ]
  * Bump Standards-Version to 4.1.0.
    + Use HTTPS in the Format field of d/copyright.
  * Fix cross build failure.  Closes: #873527
    + Drop implicitly satisfied build dependency on binutils
    + Make g++ substitutable in source/Makefile
    Thanks to Helmut Grohne <helmut@subdivi.de> for the patch.

  [ Laura Arjona Reina ]
  * Start the short description with a lowercase letter.  Closes: #873537

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 29 Aug 2017 14:49:14 +0200

ebumeter (0.4.0-3) unstable; urgency=medium

  * Sign tags.
  * Update copyright file.
  * Set dh 10.
  * Avoid useless linking.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 22 Dec 2016 00:06:19 +0100

ebumeter (0.4.0-2) unstable; urgency=medium

  * Fix VCS fields.
  * Force parallel build.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 24 Jul 2016 18:35:37 +0200

ebumeter (0.4.0-1) unstable; urgency=medium

  [ Adrian Knoth ]
  * Update homepage link (Closes: #762466)

  [ Jaromír Mikeš ]
  * Imported Upstream version 0.4.0
  * Bump Standards.
  * Add libzita-resampler-dev as build-dep.
  * Refresh patch.
  * Fix VCS fields.
  * Remove menu file.
  * Fix hardening.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 14 Jun 2016 19:11:38 +0200

ebumeter (0.2.0~dfsg0-3) unstable; urgency=low

  * Tighten build-dep on libclxclient.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 07 Aug 2013 01:33:03 +0200

ebumeter (0.2.0~dfsg0-2) unstable; urgency=low

  * Fix description. (Closes: #712399)
  * Bump standards.
  * Fix VCS canonical URLs.
  * Don't sign tags.
  * Added Keywords entry to desktop file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 16 Jun 2013 03:56:41 +0200

ebumeter (0.2.0~dfsg0-1) unstable; urgency=low

  * New upstream release.
  * Set compat/dh 9 to fix hardening.
  * Introduce doc package.
  * Update man pages.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 27 Sep 2012 10:12:43 +0200

ebumeter (0.1.0~dfsg-2) unstable; urgency=low

   [ Alessio Treglia ]
  * debian/watch: Strip ~dfsg from Debian version number.
  * wrap-and-sort -a -s
  * Update debian/copyright.
  * libpng12-dev -> libpng-dev (Closes: #662310)
  * Bump Standards.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 10 Apr 2012 23:57:37 +0200

ebumeter (0.1.0~dfsg-1) unstable; urgency=low

  * Initial release (Closes: #626533)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 12 May 2011 15:44:17 +0200
